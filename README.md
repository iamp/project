# project

Project management for the Industrial APP Marketplace (iamp).
For now this is a private project.
We want to put this project under MIT licence.


# User Stories
## App Store  #m1

As a user I want to:
* browse the app store and look for apps I need
* Find and use different categories, filters and search functions 
* See the minimum requirements for the app i want to buy
  * On what devices can it run?
* Use a modern look and feel
  * excellent user interface
* be able to buy apps
* Be able to get an offer first and hand it to my purchase department
  * Once the purchase is completed the app appears in my app list
* Have company user accounts that can share apps
  * Different users have access to one company account 
* Allow others colleagues to manage my apps
* Have a on-premise version of the store if my IT doesn’t allow my on-premise devices to connect to the store in the cloud

As an app developer I want to: 
* upload my app and enter the pricing information
* As an app developer I want to protect my apps with license management
* Upload my apps as Docker images/containers
* Upload my apps as files that can run in existing Docker containers

### Jobs
We need a specialist for eCommerce solutions to setup a store/marketplace for apps.

* find the best open source eCommerce solution
  * https://en.wikipedia.org/wiki/Comparison_of_shopping_cart_software
  * https://ofbiz.apache.org/business-users.html
  * https://www.ecomify.de/apache-ofbiz-beratung-und-dienstleistungen/
  * https://magento.com/compare-open-source-and-magento-commerce
* create the store frontend
  * .....
* License management
  * https://www.wibu.com/
* Connect the store to a Payment provider
  * https://stripe.com/en-de


## File storage #m2
* create a app storage
* Docker registry

## App Store – Device Interface #m3

As a user I want to 
* connect my devices to the Store
  * Device: see devices below: Raspberry Pi like, on premise Server or Cloud
* choose on which device to deploy the apps I purchased.


## Device management #m4

Also called Fleet management / Patch and device management

As a user I want to 
* manage all my devices
* Have shared devices for everyone in my company
* see if my firmware and OS is uptodate
* Update of Firmware and OS
* See if my APPs (Docker Images) are uptodate
* Update my APPs
* Give access to the machine provider for service 
* get to know which software version is currently running in which of my edge device
* can easily rollout software/package updates to the particular group of devices based on various parameters like name, location, current software version etc.


### Jobs 

* connect to existing platforms
   * https://www.balena.io/what-is-balena
   * https://netfield.io/
* use existing technology
   * https://www.balena.io/open/
* software update delivery platform
   * https://mkrak.org/wp-content/uploads/2018/04/FOSS-NORTH_2018_Software_Updates.pdf 
   * https://www.kynetics.com/iot-platform-update-factory
   * Eclipse hawkBit
   * Eclipse Hawkbit is a framework to manage software rollouts to edge devices connected to IP based networking infrastructure. 
   * https://www.eclipse.org/hawkbit/
   * https://medium.com/@chopra.raghav2180/iot-part1-using-hawkbit-to-update-softwares-in-raspberry-pis-475717d1bf5f


Eclipse hawkBit™ is a domain independent back-end framework for rolling out software updates to constrained edge devices as well as more powerful controllers and gateways connected to IP based networking infrastructure.
https://www.eclipse.org/hawkbit/




## Device #m5
As a device I want to:
* Push device infos to Marketplace
* Pull updates
* Connect to the marketplace

As a hardware provider I want to:
* Enable my device to connect to the marketplace
   * Push device infos to Marketplace
* Allow my device firmware and OS to be updated by the marketplace


### Jobs
We need a specialist for embedded devices to create the clients to run on most devices.

* integrate rauc & hawkbit on the devices
   * https://www.pengutronix.de/de/blog/2017-11-21-rauc-hawkbit.html
* create a yocto os for raspberry
    * https://jumpnowtek.com/rpi/Raspberry-Pi-Systems-with-Yocto.html


RAUC Image for Full system Update
Opkg for Modules and Apps
Docker Containers

RAUC controls the update process on embedded Linux systems. It is both a target application that runs as an update client and a host/target tool that allows you to create, inspect and modify installation artifacts.
https://github.com/rauc/rauc

RAUC hawkBit Client
The RAUC hawkBit client is a simple python-based library and example application that runs on your target and operates as an interface between the RAUC D-Bus API and the hawkBit DDI API.
https://github.com/rauc/rauc-hawkbit#rauc-hawkbit-client

opkg (open package management) is a lightweight package management system based upon ipkg. It is written in C and resembles Advanced Package Tool (APT)/dpkg in operation. It is intended for use on embedded Linux devices.
https://git.yoctoproject.org/cgit/cgit.cgi/opkg/


Device Side Client (DDI)
Hawkbit provides us with an API which can be used by device (such as rpi) to fetch software updates which are deployed (or rolled out) by Hawkbit Update Server.

# Devices 
Linux based embedded devices
Linux OS: Yocto or Raspbian

* https://www.yoctoproject.org/
* http://www.raspbian.org/

The MVP should work with Raspberry Pis with Raspbian and Docker 

## Raspberry Pi based
Yocto / Raspbian

* Raspberry Pi 
    * https://www.raspberrypi.org/
* emPC-A/RPI3
    * https://www.janztec.com/devices/embedded-computer/empc-arpi3/
* RevPi Core 3+
    * https://revolution.kunbus.de/
* Hilscher netPI
    * Yocto
    * https://www.netiot.com/de/netpi/industrial-raspberry-pi-3/

## ARM based
Yocto

* Phoenix Contact PLCnext AXC F 2152
    * https://www.plcnext-community.net/en/
* Phoenix Contact Axioline Smart Elements
    * https://www.phoenixcontact.com/online/portal/de?urile=wcm%3apath%3a/dede/web/main/products/subcategory_pages/Axioline_Smart_Elements_P-21-11-06/ffb516f4-0f15-441c-842f-ef9102a5781b
* Phoenix Contact ILC 2050 BI
    * https://www.sysmik.de/
* Harting Mica
    * Beaglebone based
    * https://www.harting-mica.com/de
* WAGO PFC200 Controller
    * https://www.wago.com/de/automatisierungstechnik/sps-entdecken/pfc200
* Weidmüller UC20-WL2000-IOT
    * https://catalog.weidmueller.com/procat/Product.jsp;jsessionid=D72A88424AA5383B8BCBF05D8FEC1D37?productId=(%5b1334990000%5d)&groupId=(%22group25584720478584%22)&page=Product

## Intel based

* Simatic IoT2040  / Arduino compatible
   * IOT2020/2040 is based on the Intel Quark® x1000/x1020-CPU, the Yocto Linux open source operating system and a Software Development Kit (SDK) from Siemens for implementation of projects in high-level languages.
   * https://mall.industry.siemens.com/mall/de/WW/Catalog/Product/6ES7647-0AA00-1YA2
   * https://new.siemens.com/global/en/company/sustainability/education/sce/iot2000.html
   * The IOT2020 ships at € 46,88  https://at.rs-online.com/web/p/iot-entwicklungskit/1244037/?searchTerm=1244037

# x86 Server based (on premise)

* Red Hat Enterprise Linux (RHEL) with docker
   * https://www.redhat.com/de/technologies/linux-platforms/enterprise-linux
* Windows Server 2016 with docker
   * Kubernetes cluster based
   * https://kubernetes.io/
   * Production-Grade Container Orchestration
   * Automated container deployment, scaling, and management
* Cloud based
    * AWS
    * AZURE



# APPs

* https://www.iobroker.net/


# JSONs
Some examples about possible data

* https://www.eclipse.org/hawkbit/apis/management_api/

```
{
device_name : device1
device_type : rpi3
os_type : yocto
ip_adress : 192.168.0.34
location : 3908
Installed_apps: [ app1v1, app3v3, app5v6]
device_configuration : config_1
functions : [ edit_device, update_device, enter_device ]
key_sensors : [temperature, humidity] 
}

{
location_name :
}


{
app_name : app1
app_description : this app is for this
pricing : 
supported_systems : []
pictures : 
functions : request_quote, buy, install
type : docker_container
}


{ 
os_name :
type : firmware
Version
Supported_devices : 
file_location :
}
```
    
